# Lib

## Inicialização 

Framework

### Pre requisitos

* Node.js 12.12+

## VARIABLE

```
ENVIRONMENT = Ambiente;
NAME = Nome da Aplicação;
NODE_PATH = NODE_PATH;
PORT = Porta em que o serviço vai rodar;
URL = Url em que o serviço vai responder;
FILE_SIZE_POST_MB = Tamanho máximo de um post;
DEBUG = Biblioteca Log debug;
DEBUG_POST = Print dos requests;
REQUEST_TIME_OUT = Tempo para uma execução falhar
CORS = Habilitar cors
```


## Development
