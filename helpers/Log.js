const clc = require('cli-color');

class Log {

    error(args) {
        console.log(`${clc.red(`ERROR`)}: ${JSON.stringify(args)}`);
    }

    warning(args) {
        console.log(`${clc.yellow(`ERROR`)}: ${JSON.stringify(args)}`);
    }

    normal(args) {
        console.log(`${clc.blue(`NORMAL`)}: ${JSON.stringify(args)}`);
    }

    debug(args) {
        if (process.env.DEBUG === 'true')
            console.log(`${clc.green(`NORMAL`)}: ${JSON.stringify(args)}`);
    }
}


module.exports = new Log();
