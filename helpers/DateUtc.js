const moment = require('moment');

class DateUtc {

    /**
     * Now
     * @returns {number}
     */
    now() {
        let date = moment().format("YYYYMMDDHHmmss");
        return parseInt(moment(date, "YYYYMMDDHHmmss").utc().format("YYYYMMDDHHmmss"));
    }

    add(val, type) {
        let date = moment();
        date.add(val, type);
        return parseInt(moment(date).utc().format("YYYYMMDDHHmmss"));
    }

    /**
     * Format
     * @param val
     * @param format
     * @returns {*|moment.Moment}
     */
    moment(val, format = null) {
        return (!format) ? moment(val) : moment(val, format) ;
    }

    /**
     *
     * @param val
     * @returns {number}
     */
    dateHourToUtc(val) {
        let date = moment(val, "YYYY-MM-DD HH:mm");
        return parseInt(moment(date, "YYYYMMDDHHmmss").format("YYYYMMDDHHmmss"));
    }

    isValid(val) {
        return moment(val, 'YYYYMMDDHHmmss').isValid();
    }

    intToUtc(val) {
        return parseInt(moment(val).format('YYYYMMDDHHmmss'));
    }

    /**
     * YYYY-MM-DD to YYYYMMDDHHmmss
     * @param val
     * @returns {number}
     */
    humanToUtc(val) {
        return parseInt(moment(val, 'YYYY-MM-DD').format('YYYYMMDDHHmmss'));
    }

    /**
     * YYYYMMDDHHmmss to YYYY-MM-DD
     * @param val
     * @returns {string}
     */
    utcToHuman(val) {
        return moment(val, "YYYYMMDDHHmmss").format('YYYY-MM-DD');
    }

    /**
     * YYYYMMDDHHmmss to DD/MM/YYYY
     * @param val
     * @returns {string}
     */
    utcToHumanBR(val) {
        return moment(val, "YYYYMMDDHHmmss").format('DD/MM/YYYY');
    }

    /**
     * YYYY-MM-DD
     * @param val
     * @returns {number}
     */
    startDate(val) {
        return parseInt(this.moment(val, 'YYYY-MM-DD').hours(0).minutes(0).second(0).format('YYYYMMDDHHmmss'));
    }

    /**
     * YYYY-MM-DD
     * @param val
     * @returns {number}
     */
    endDate(val) {
        return parseInt(this.moment(val, 'YYYY-MM-DD').hours(23).minutes(59).second(59).format('YYYYMMDDHHmmss'));
    }
}


module.exports = new DateUtc();
