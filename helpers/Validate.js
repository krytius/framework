class Validate {

    /**
     * Validations fields request
     * @param fields
     * @param body
     */
    requireFields(fields = [], body = {}) {
        for (let i = 0; i < fields.length; i++) {
            if (!body[fields[i]] || body[fields[i]] === '' || body[fields[i]] === 0 || body[fields[i]] == null) {
                throw `${fields[i]} required`;
            }
        }
    }


}


module.exports = new Validate();
