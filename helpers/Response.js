const Log = require("./Log");

class Response {

    success(res, data = {}, message = "OK") {

        if (process.env.DEBUG_POST !== "false") {
            Log.normal({
                url: res.req.url,
                data: data
            });
        }

        this.json(res, 200, message, data);
    }

    error(res, message, messageLog) {

        if (messageLog) {
            Log.error(messageLog);
        }

        this.json(res, 500, (message && message.message) ? message.message : message);
    }

    json(res, code, message = "OK", data = {}) {
        if(!res.finished) {
            res.status(code);
            res.set("Connection", "close");
            res.send({
                code: code,
                data: data,
                message: message
            });
        }
    }

    file(res, file, filename) {
        if(!res.finished) {
            res.set("Connection", "close");
            res.download(file, filename);
            res.end();
            res.connection.destroy();
        }
    }

}


module.exports = new Response();
