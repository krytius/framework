const CryptoJS = require('crypto-js');

class Encrypt {

    encrypt(value) {
        return CryptoJS.AES.encrypt(value, process.env.KEY).toString();
    }

    decrypt(value) {
        return CryptoJS.AES.decrypt(value, process.env.KEY).toString(CryptoJS.enc.Utf8);
    }

}


module.exports = new Encrypt();
