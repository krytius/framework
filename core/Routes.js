const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const expressLayouts = require("express-ejs-layouts");
const fs = require("fs");
const path = require("path");
const clc = require("cli-color");
const Params = require("./Params");

class Routes {

    rawBodySaver(req, res, buf) {
        if (buf && buf.length) {
            req.rawBody = buf.toString();
        }
    }

    typeParser(req) {
        if (req.get("content-type")) {
            return req.get("content-type").indexOf("multipart/form-data") !== 0;
        }
    }

    init() {
        return new Promise(async (resolve) => {
            let app = express();
            if (Params.CORS) {
                app.use(cors());
            }
            app.set("view engine", "ejs");
            app.set("views", path.join(Params.DIR_SRC, "views"));
            app.use(expressLayouts);
            app.use(bodyParser.json({
                verify: this.rawBodySaver,
                extended: true,
                limit: `${Params.FILE_SIZE_POST_MB}mb`
            }));
            app.use(bodyParser.urlencoded({
                verify: this.rawBodySaver,
                extended: true,
                limit: `${Params.FILE_SIZE_POST_MB}mb`,
                parameterLimit: 100000
            }));
            app.use(bodyParser.raw({
                verify: this.rawBodySaver,
                type: this.typeParser,
                limit: `${Params.FILE_SIZE_POST_MB}mb`,
                extended: true
            }));
            app.use(express.static(path.join(Params.DIR_SRC, "public")));

            let pathController = path.join(Params.DIR_SRC, `controllers`);
            let files = fs.readdirSync(pathController);

            console.log(`Routes`);

            let filesPath = [];
            for (let i = 0; i < files.length; i++) {
                let fullPath = path.join(pathController, files[i]);

                if (fs.lstatSync(fullPath).isDirectory()) {
                    filesPath = this.mountRoute(fullPath, filesPath);
                } else {
                    if (path.extname(files[i]) !== ".js") {
                        continue;
                    }
                    filesPath.push(path.join(`/`, files[i].replace(".js", "")));
                }
            }

            let defaultMiddleware = (req, res, next) => {
                next();
            };

            for (let i = 0; i < filesPath.length; i++) {

                let fileItem = filesPath[i].replace(`${pathController}`, "");
                let RouteItem = require(`${pathController}${fileItem}`);
                let route = new RouteItem(app);
                let routeName = fileItem.toLowerCase().split("\\").join("/");

                if (typeof (route.get) === "function") {
                    app.get(`${routeName}`, (route.middleware) ? route.middleware : defaultMiddleware, (req, res) => route.get(req, res));
                    console.log(`${clc.green(`GET`)}    http://${Params.URL}:${Params.PORT}${routeName}`);
                }
                if (typeof (route.getItem) === "function") {
                    app.get(`${routeName}/:id`, (route.middleware) ? route.middleware : defaultMiddleware, (req, res) => route.getItem(req, res));
                    console.log(`${clc.green(`GET`)}    http://${Params.URL}:${Params.PORT}${routeName}/:id`);
                }
                if (typeof (route.post) === "function") {
                    app.post(`${routeName}`, (route.middleware) ? route.middleware : defaultMiddleware, (req, res) => route.post(req, res));
                    console.log(`${clc.yellow(`POST`)}   http://${Params.URL}:${Params.PORT}${routeName}`);
                }
                if (typeof (route.put) === "function") {
                    app.put(`${routeName}/:id`, (route.middleware) ? route.middleware : defaultMiddleware, (req, res) => route.put(req, res));
                    console.log(`${clc.blue(`PUT`)}    http://${Params.URL}:${Params.PORT}${routeName}/:id`);
                }
                if (typeof (route.delete) === "function") {
                    app.delete(`${routeName}/:id`, (route.middleware) ? route.middleware : defaultMiddleware, (req, res) => route.delete(req, res));
                    console.log(`${clc.red(`DELETE`)} http://${Params.URL}:${Params.PORT}${routeName}/:id`);
                }
            }

            let RouteIndexItem = require(`${pathController}/Index.js`);
            let routeIndex = new RouteIndexItem(app);

            if (typeof (routeIndex.get) === "function") {
                app.get(`/`, (routeIndex.middleware) ? routeIndex.middleware : defaultMiddleware, (req, res) => routeIndex.get(req, res));
                console.log(`${clc.green(`GET`)}    http://${Params.URL}:${Params.PORT}/`);
            }
            if (typeof (routeIndex.getItem) === "function") {
                app.get(`/:id`, (routeIndex.middleware) ? routeIndex.middleware : defaultMiddleware, (req, res) => routeIndex.getItem(req, res));
                console.log(`${clc.green(`GET`)}    http://${Params.URL}:${Params.PORT}/:id`);
            }

            app.listen(Params.PORT, () => {
                console.log(`Server run http://${Params.URL}:${Params.PORT}`);
                resolve();
            });
        });
    }

    mountRoute(dir, filesPath) {
        let filesArray = fs.readdirSync(dir);
        for (let key in filesArray) {
            let file = filesArray[key];
            let fullPath = path.join(dir, file);
            if (fs.lstatSync(fullPath).isDirectory()) {
                filesPath = this.mountRoute(fullPath, filesPath);
            } else {
                if (path.extname(file) !== ".js") {
                    continue;
                }
                filesPath.push(`${dir}/${file.replace(".js", "")}`);
            }
        }

        return filesPath;
    }

}

module.exports = Routes;
