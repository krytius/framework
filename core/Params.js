class Params {

    // CONFIG
    NODE_PATH = process.env.NODE_PATH ? process.env.NODE_PATH : './';
    DIR_SRC = process.env.NODE_PATH;
    ENVIRONMENT = process.env.ENVIRONMENT ? process.env.ENVIRONMENT : "dev";
    NAME = process.env.NAME;
    PORT = process.env.PORT ? process.env.PORT : 9000;
    URL = process.env.URL ? process.env.URL : "localhost";
    FILE_SIZE_POST_MB = process.env.FILE_SIZE_POST_M ? process.env.FILE_SIZE_POST_M : 10;
    DEBUG = process.env.DEBUG ? eval(process.env.DEBUG) : true;
    DEBUG_POST = process.env.DEBUG_POST ? eval(process.env.DEBUG_POST) : true;
    REQUEST_TIME_OUT = process.env.REQUEST_TIME_OUT ? process.env.REQUEST_TIME_OUT : 30;
    CORS = process.env.CORS ? eval(process.env.CORS) : true;

    constructor() {
    }

}

module.exports = new Params();