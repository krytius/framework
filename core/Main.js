const Routes = require('./Routes');

class Main {

    constructor() {
        this.init().then(() => {
            console.log('Program initialized.');
        });
    }

    async init() {
        console.log('Program start.');
        let routes = new Routes();
        await routes.init();
        return true;
    }
}

module.exports = Main;
